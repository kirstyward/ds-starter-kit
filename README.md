# DS Starter Kit

A series of starter point python and R notebooks for a variety of Data Science topics and technology I've worked with.

### Currently Available

* R
	* Data Table
* Python
	* NetworkX / Graph Analytics basics


### Planned Notebooks

* Prophet forecasting (R & Python)
* ggplot2
* tidyverse

